#!/usr/bin/php -q
<?php
/**
 * Prime number search.
 *
 * @author    Sandor Papp <spapp@spappsite.hu>
 * @copyright 2013 Sandor Papp
 * @license   http://creativecommons.org/licenses/by-nc-nd/3.0/deed.hu Creative Commons Nevezd meg! - Ne add el! - Ne változtasd! 3.0
 * @package   prime_number
 * @version   1.0.1
 */
define('APPLICATION_PATH', dirname(dirname(__FILE__)));

require(APPLICATION_PATH . '/library/PrimeNumber.php');

if ('cli' !== substr(php_sapi_name(), 0, 3)) {
    new Exception('Use this script in CLI.');
}

// Remove zero argument.
array_shift($argv);

$option = array_shift($argv);

if (intval($option)) {
    print_r(PrimeNumber::getInstance()->getNumbers(intval($option)));
} elseif (file_exists(APPLICATION_PATH . '/doc/man.txt')) {
    echo file_get_contents(APPLICATION_PATH . '/doc/man.txt');
}

exit(0);
