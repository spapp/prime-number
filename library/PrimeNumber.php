<?php
/**
 * Prime number search.
 *
 * @author    Sandor Papp <spapp@spappsite.hu>
 * @copyright 2013 Sandor Papp
 * @license   http://creativecommons.org/licenses/by-nc-nd/3.0/deed.hu Creative Commons Nevezd meg! - Ne add el! - Ne változtasd! 3.0
 * @package   prime_number
 * @version   1.0.1
 */
/**
 * Class PrimeNumber
 *
 * @link http://hu.wikipedia.org/wiki/Pr%C3%ADmsz%C3%A1mok
 */
class PrimeNumber {
    /**
     * Cache file glue.
     */
    const GLUE = ',';

    /**
     * Cache file name.
     */
    const CACHE_FILE_NAME = 'PRIME_NUMBERS_8E9MLI';

    /**
     * PrimeNumber instance.
     *
     * @var null|PrimeNumber
     */
    protected static $instance = null;

    /**
     * Contains the prime numbers.
     *
     * @var array prime numbers
     */
    protected $primeNumbers = array();

    /**
     * Constructor.
     *
     * @protected
     */
    protected function __construct() {
    }

    /**
     * Returns class instance.
     *
     * @return null|PrimeNumber
     */
    public static function getInstance() {
        if (null === self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Returns the prime numbers from two to given number.
     *
     * If $aWithCache is TRUE then use cached data.
     *
     * @param int  $aMaxNumber
     * @param bool $aWithCache
     *
     * @return array prime numbers
     */
    public function getNumbers($aMaxNumber, $aWithCache = true) {
        if (true === $aWithCache) {
            $this->readCache();
        }

        if ($this->getLast() > $aMaxNumber) {
            return $this->findInCache($aMaxNumber);
        }

        $this->find(($this->getLast() + 1), $aMaxNumber)->writeCache();

        return $this->primeNumbers;
    }

    /**
     * Tells whether it is a valid prime number or no.
     *
     * @param int $aNumber
     *
     * @return bool true if it is a valid prime number
     */
    public static function isPrime($aNumber) {
        $number = (int)$aNumber;

        if ($number === 2) {
            return true;
        }

        if ($number < 2 or $number % 2 === 0) {
            return false;
        }

        $half = (int)($number / 2);

        for ($i = 3; $i < $half; $i++) {
            if ($number % $i === 0) {
                return false;
            }
        }

        return true;
    }

    /**
     * Returns directory path used for temporary files.
     *
     * @return string
     */
    protected function getCacheFileName() {
        return sys_get_temp_dir() . DIRECTORY_SEPARATOR . self::CACHE_FILE_NAME;
    }

    /**
     * Read cached prime numbers.
     *
     * @return $this
     */
    protected function readCache() {
        if (file_exists($this->getCacheFileName())) {
            $numbers = file_get_contents($this->getCacheFileName());
            if ($numbers) {
                $this->primeNumbers = explode(self::GLUE, $numbers);
            }
        }

        return $this;
    }

    /**
     * Write calculate prime numbers from the cache file.
     *
     * @return $this
     */
    protected function writeCache() {
        file_put_contents($this->getCacheFileName(), implode(self::GLUE, $this->primeNumbers));

        return $this;
    }

    /**
     * Returns last prime number.
     *
     * @return int
     */
    protected function getLast() {
        if (count($this->primeNumbers) > 0) {
            return (int)$this->primeNumbers[count($this->primeNumbers) - 1];
        }

        return 2;
    }

    /**
     * Finds the prime numbers between the two parameters.
     *
     * @param int $aStart
     * @param int $aMaxNumber
     *
     * @return $this
     */
    protected function find($aStart, $aMaxNumber) {
        for ($i = $aStart; $i < $aMaxNumber; $i++) {
            if (true === self::isPrime($i)) {
                array_push($this->primeNumbers, $i);
            }
        }

        return $this;
    }

    /**
     * Finds the prime numbers in the cache.
     *
     * @param int $aMaxNumber
     *
     * @return array
     */
    protected function findInCache($aMaxNumber) {
        for ($i = 0; $i < count($this->primeNumbers); $i++) {
            if ($this->primeNumbers[$i] > $aMaxNumber) {
                break;
            }
        }

        return array_slice($this->primeNumbers, 0, $i);
    }
}